%%%%%%%%%%%%%%%%%%%%%%%%% Initial Setup %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%> Feature track veridicality
clc; clear all; close all;
% Setup VLFeat
run('/Users/qiwuzhang/Downloads/vlfeat-0.9.21/toolbox/vl_setup.m');
%%%%%%%%%%%%%%%%%%%%%%%%% Initial Setup %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%> define parameters

%%%%%%%%%%% KITTI   %%%%%%%%%%%%%%%%%%
%datasetName = 'KITTI';
%sequenceName = '03';
%K = [718.856, 0, 607.193; 0, 718.86, 185.216; 0, 0, 1]; %> intrinsic matrix
%category_left = '/image_1/'; %the images we are actually using
%category_right = '/image_0/';

%%%%%%%%%%% EuRoC %%%%%%%%%%%%%%%%%%%
datasetName = 'EuRoC';
sequenceName = 'MH_01_easy';
category_left = '/cam0/'; %the images we are actually using
category_right = '/cam1/';
K = [458.654, 0, 367.215; 0, 457.296, 248.375; 0, 0, 1]; %> intrinsic matrix

featureType = "vlfeat-SIFT";
filePostFix = '.txt';

invK = inv(K);

%> vlfeat parameters
vlfeat_params = [];
vlfeat_params.SiftThreshPeak = 10;                              % SIFT parameter
vlfeat_params.SiftThreshEdge = 20;                             % SIFT parameter
vlfeat_params.ratioTestRatio = 1.33;                            % SIFT parameter, 1/0.75 = 1.333


%> Settings
PARAMS.INLIER_REPROJ_ERR_THRESH = 2;

PARAMS.DISPLAY_MATCHES_FOR_DEBUG     = 0;
PARAMS.DRAW_INLIER_RATIO_TRACKLENGTH = 0;
PARAMS.DRAW_TRACKS = 1;
PARAMS.DRAW_EPIPOLAR_LINE = 0;

%> Function Macro for constructing a skew-symmetric matrix
skew_T = @(T)[0, -T(3,1), T(2,1); T(3,1), 0, -T(1,1); -T(2,1), T(1,1), 0];


%%%%%%%%%%%%%%%%%%%%%%%%% Dataset and Image Handling %%%%%%%%%%%%%%%%%%%

%> read all R, T, time stamp, and image directories of each frame, both
%> left and right at 05 both have 2761 images
[all_T, all_R, time_per_fr, imgFileName] = readDataset(datasetName, sequenceName, category_left);
[~, ~, ~, imgFileName_Right] = readDataset(datasetName, sequenceName, category_right);

seq_start = 1; %> must greater than 1
seq_end = 3600; %2761-25?


%%%%%%%%%%%%%%%%%%%%%%%% Feature Tracking Initialization %%%%%%%%%%%%%%%%%%%%%%%%
%> initialize tracks
covisible_frames = 25;
e_3 = [0,0,1];
use_monocular = 0;

if featureType == 'vlfeat-SIFT'
    collection_featureType = 'SIFT';
else
    collection_featureType = featureType;
end

currentFileDirectory = '/Users/qiwuzhang/Desktop/Brown/LEMS/code/';
datasequenceName = datasetName +  "-seq"  + sequenceName + "/";
folderName = strcat(currentFileDirectory, datasequenceName);
status = mkdir(folderName);

if status == 1
    disp(['Folder "', folderName, '" was created successfully.']);
else
    disp(['Failed to create folder "', folderName, '".']);
end
fileName = 'num-of-tracks-per-length-';
fullOutputFileName = strcat(folderName, fileName, collection_featureType, filePostFix);
disp(fullOutputFileName);
printToFile = fopen(fullOutputFileName, 'w');

inlier_ratio_fileName = 'inlier_Ratio_';
inlier_ratio_fullOutputFileName = strcat(folderName, inlier_ratio_fileName, collection_featureType, filePostFix);
disp(inlier_ratio_fullOutputFileName);
inlier_ratio_printToFile = fopen(inlier_ratio_fullOutputFileName, 'w');

%> loop from minimal covisible frames to maximal covisible frames
current_bidirectional_inlier_ratio = zeros(covisible_frames-1,1);
window_idx = 0;

%> collect numbers of N covisible frames
collect_current_num_of_track_length = zeros(covisible_frames-1, 1);
collect_total_num_of_track_length = zeros(size(seq_start:seq_end, 2), covisible_frames-1);

%> set camera coordinate of R and T
R_cc = zeros(3,3,covisible_frames);
T_cc = zeros(3,1,covisible_frames);
round = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Main Processing Loop %%%%%%%%%%%%%%%%%%%%%%%%%%

% The outer loop iterates over the entire sequence of images
for start_fr = seq_start:seq_end
    window_idx = window_idx + 1;
    fprintf(". ");
    if mod(start_fr, 100) == 0
        fprintf("round %d ", round);
        fprintf("\n");
        round = round + 1;
    end

    % The inner loop processes a window of frames of size 25 for feature tracking.
    for i = start_fr:start_fr+covisible_frames-1
        %> plot at the last image if there exists features that survived 25
        %> images
        %{
        if i == start_fr+24
            feature_found = size(covisible_feature_tracks,1);
            if PARAMS.DRAW_TRACKS && feature_found>1
                disp('\nFound continuous feature\n');
                self_drawFeatureTracks(imgFileName(start_fr,1), covisible_frames, covisible_feature_tracks, covisible_gamma_tracks, -1);
            end
        end
        %}
        %> detect and extract features from 25 frames, starting from start_fr

        if i == start_fr
            [featureDescriptions, featurePoints, ~, imgCols] = self_feature_extractor(imgFileName(i,1), featureType, i, start_fr, datasetName, K);
            cov_indx = 1;
            f1_des = featureDescriptions;
            f1_pts = featurePoints;

            %> initialize covisible feature tracks
            covisible_feature_tracks = zeros(size(f1_pts, 1), 2, covisible_frames);
            %> Matched feature points are used to update the feature tracks 
            if strcmp(featureType, 'vlfeat-SIFT') || strcmp(featureType, 'TAPIR')
                covisible_feature_tracks(:,:,cov_indx) = f1_pts(:,1:2);
            else
                covisible_feature_tracks(:,:,cov_indx) = f1_pts.Location;
            end

            %> initialize covisible gamma tracks
            covisible_gamma_tracks = zeros(size(f1_pts, 1), 3, covisible_frames);
            covisible_gamma_tracks(:,1:2,cov_indx) = covisible_feature_tracks(:,1:2,cov_indx);

            %> retreive R_0 and T_0 at world coordinate
            R_0 = all_R(:,:,i);
            T_0 = all_T(:,i);
            R1 = R_0;
            T1 = T_0;

            %> make R_0 and T_0 be at the camera coordinate
            R_cc(:,:,cov_indx) = eye(3);
            T_cc(:,:,cov_indx) = [0; 0; 0];

            cov_indx = cov_indx + 1;
            continue;
        else
            
            [featureDescriptions, featurePoints, ~, ~] = self_feature_extractor(imgFileName(i,1), featureType,i, start_fr, datasetName, K);
            
            f2_des = featureDescriptions;
            f2_pts = featurePoints;

            %> do feature matching with the first image in the covisible
            %> frames
            if strcmp(featureType, 'vlfeat-SIFT')
                [matches, scores] = vl_ubcmatch(f1_des', f2_des', vlfeat_params.ratioTestRatio);
                matchedPoints1 = f1_pts(matches(1, :), 1:2);
                matchedPoints2 = f2_pts(matches(2, :), 1:2);
            else
                indexPairs = matchFeatures(f1_des,f2_des);
                if strcmp(featureType, 'TAPIR')
                    matchedPoints1 = f1_pts; 
                    matchedPoints2 = f2_pts;
                else
                    matchedPoints1 = f1_pts(indexPairs(:,1)).Location;   %> not useful in current scope
                    matchedPoints2 = f2_pts(indexPairs(:,2)).Location;
                end
            end

            % ================== FEATURE TRACKS ===================
            %> Matched feature points are used to update the feature tracks 
            if strcmp(featureType, 'vlfeat-SIFT')
                %> delete unmatched features from prior frames
                covisible_feature_tracks = covisible_feature_tracks(matches(1, :),:,:);
                f2_des = f2_des(matches(2, :),:);
                f2_pts = f2_pts(matches(2, :),:);
                %> store the current matched features
                covisible_feature_tracks(:,:,cov_indx) = f2_pts(:,1:2);
            else
                if ~strcmp(featureType, 'TAPIR')
                    %> delete unmatched features from prior frames
                    covisible_feature_tracks = covisible_feature_tracks(indexPairs(:,1),:,:);
                    f2_des = f2_des(indexPairs(:,2),:);
                    f2_pts = f2_pts(indexPairs(:,2),:);
                    %> store the current matched features
                    covisible_feature_tracks(:,:,cov_indx) = matchedPoints2;
                end
            end

            %> record number of covisible tracks
            collect_current_num_of_track_length(cov_indx-1, 1) = size(covisible_feature_tracks, 1);
            
            %> Forward direction: find inliers of the current frame using 
            %  the epipolar line constructed from the previous frame
            %> (i) Compute the Relative Pose
            R2 = all_R(:,:,i);
            T2 = all_T(:,i);
            Rel_R = R2' * R1;
            Rel_T = R2' * (T1 - T2);
            E21 = skew_T(Rel_T) * Rel_R;
            F21 = invK' * E21 * invK;
            x1 = matchedPoints1';
            
            %> (ii) Compute the epipolar line coefficients
            Apixel_21 = F21(1,:) * [x1; ones(1, size(x1, 2))];
            Bpixel_21 = F21(2,:) * [x1; ones(1, size(x1, 2))];
            Cpixel_21 = F21(3,:) * [x1; ones(1, size(x1, 2))];

            %> (iii) Find the inliers
            matchedPoints2 = matchedPoints2';
            A_xi  = Apixel_21.*matchedPoints2(1,:);
            B_eta = Bpixel_21.*matchedPoints2(2,:);
            numerOfDist = abs(A_xi + B_eta + Cpixel_21);
            denomOfDist = Apixel_21.^2 + Bpixel_21.^2;
            denomOfDist = sqrt(denomOfDist);
            dist21 = numerOfDist./denomOfDist;
            inlier_indices_forward = find(dist21 <= PARAMS.INLIER_REPROJ_ERR_THRESH);
            num_inliers_forward = length(inlier_indices_forward);
            
            forward_inlier_ratio = num_inliers_forward/size(covisible_feature_tracks, 1);
            
            if PARAMS.DISPLAY_MATCHES_FOR_DEBUG == 1
                %> Display settings
                disp_Img_SideBySide = 0;
                disp_Match_Lines    = 0;
                disp_Epipolar_Lines = 1;
                disp_With_Text      = 0;
                disp_Num_Inliers = min(20, num_inliers_forward);
                img1 = imread(imgFileName(i-1,1));
                img2 = imread(imgFileName(i,  1));
                self_draw_epipolar_lines(img1, img2, matchedPoints1', matchedPoints2, F21, PARAMS.INLIER_REPROJ_ERR_THRESH, ...
                                         disp_Img_SideBySide, disp_Match_Lines, disp_Epipolar_Lines, disp_Num_Inliers, disp_With_Text);
            end
            
            %> Backward direction: find inliers of the previous frame using
            %  the epipolar line constructed by the current frame
            %> (i) Compute the Relative Pose
            Rel_R = R1' * R2;
            Rel_T = R1' * (T2 - T1);
            E12 = skew_T(Rel_T) * Rel_R;
            F12 = invK' * E12 * invK;
            x2 = matchedPoints2;
            
            %> (ii) Compute the epipolar line coefficients
            Apixel_12 = F12(1,:) * [x2; ones(1, size(x1, 2))];
            Bpixel_12 = F12(2,:) * [x2; ones(1, size(x1, 2))];
            Cpixel_12 = F12(3,:) * [x2; ones(1, size(x1, 2))];
            
            %> (iii) Find the inliers
            matchedPoints1 = matchedPoints1';
            A_xi  = Apixel_12.*matchedPoints1(1,:);
            B_eta = Bpixel_12.*matchedPoints1(2,:);
            numerOfDist = abs(A_xi + B_eta + Cpixel_12);
            denomOfDist = Apixel_12.^2 + Bpixel_12.^2;
            denomOfDist = sqrt(denomOfDist);
            dist12 = numerOfDist./denomOfDist;
            inlier_indices_backward = find(dist12 <= PARAMS.INLIER_REPROJ_ERR_THRESH);
            num_inliers_backward = length(inlier_indices_backward);

            min_num_inliers = min(num_inliers_forward, num_inliers_backward);
            current_bidirectional_inlier_ratio(cov_indx-1, window_idx) = min_num_inliers/size(covisible_feature_tracks, 1);
            
            %> switch f2 to f1
            f1_des = f2_des;
            f1_pts = f2_pts;
            
            R1 = R2;
            T1 = T2;
            
            cov_indx = cov_indx + 1;
        end
    end
    
    collect_total_num_of_track_length(start_fr-seq_start+1, :) = collect_current_num_of_track_length(:,1)';
    
    %> write to the file simultneously

    for i = 1:size(collect_current_num_of_track_length, 1)
        wr_number = num2str(collect_current_num_of_track_length(i,1));
        fprintf(printToFile, wr_number);
        fprintf(printToFile, '\t');
    end
    fprintf(printToFile, '\n');

    for i = 1:size(current_bidirectional_inlier_ratio, 1)
        wr_number = current_bidirectional_inlier_ratio(i,window_idx);
        if isnan(wr_number)
            wr_number = 0;
        end
        wr_number = num2str(wr_number);
        fprintf(inlier_ratio_printToFile, wr_number);
        fprintf(inlier_ratio_printToFile, '\t');
    end
    fprintf(inlier_ratio_printToFile, '\n');

  
end

fprintf("\n");

%> I commented out the below code 2 years ago...

%fprintf("end-point inlier ratio: %f\n", min(inlierRatio_forward, inlierRatio_backward));

%     % ================== DISTANCE TO EPIPOLAR LINE =====================
%     %> compute coefficients of an epipolar line
%     A = zeros(size(E, 3), size(covisible_feature_tracks, 1));
%     B = zeros(size(E, 3), size(covisible_feature_tracks, 1));
%     C = zeros(size(E, 3), size(covisible_feature_tracks, 1));
%     feat1 = ones(3,size(covisible_feature_tracks, 1));
%     feat2 = feat1;
%     dist12 = zeros(size(E, 3), size(feat1, 2));
%     dist21 = zeros(size(E, 3), size(feat1, 2));
%     denomOfDist = zeros(size(E, 3), size(feat1, 2));
%     numerOfDist = zeros(size(E, 3), size(feat1, 2));
%     A_ep = zeros(size(E, 3), size(feat1, 2));
%     B_it = zeros(size(E, 3), size(feat1, 2));
%     
%     %> loop over all essential matrices
%     for p = 1:covisible_frames-1
%         
%         feat1(1,:) = covisible_feature_tracks(:,1,p)';
%         feat1(2,:) = covisible_feature_tracks(:,2,p)';
%         feat2(1,:) = covisible_feature_tracks(:,1,p+1)';
%         feat2(2,:) = covisible_feature_tracks(:,2,p+1)';
%         
%         calE = inv(K)' * E(:,:,p) * inv(K);
%         A(p, :) = calE(1,:) *feat1;
%         B(p, :) = calE(2,:) *feat1;
%         C(p, :) = calE(3,:) *feat1;
%         
%         %> compute the distance from the features to the epipolar line
%         for k = 1 : size(feat1, 2)
%             A_ep(:,k) = A(:,k).*feat2(1, k);    %> epsilon
%             B_it(:,k) = B(:,k).*feat2(2, k);    %> ita
%         end
%         numerOfDist = abs(A_ep + B_it + C);
%         denomOfDist = A.^2 + B.^2;
%         denomOfDist = sqrt(denomOfDist);
%         dist12 = numerOfDist./denomOfDist;
%     end
%     
%     %> reverse directional epipolar line    
%     %> loop over all essential matrices
%     for p = 1:covisible_frames-1
%         feat1(1,:) = covisible_feature_tracks(:,1,p)';
%         feat1(2,:) = covisible_feature_tracks(:,2,p)';
%         feat2(1,:) = covisible_feature_tracks(:,1,p+1)';
%         feat2(2,:) = covisible_feature_tracks(:,2,p+1)';
%         
%         calE = inv(K)' * revE(:,:,p) * inv(K);
%         A(p, :) = calE(1,:) *feat2;
%         B(p, :) = calE(2,:) *feat2;
%         C(p, :) = calE(3,:) *feat2;
%         
%         %> compute the distance from the features to the epipolar line
%         for k = 1 : size(feat1, 2)
%             A_ep(:,k) = A(:,k).*feat1(1, k);    %> epsilon
%             B_it(:,k) = B(:,k).*feat1(2, k);    %> ita
%         end
%         numerOfDist = abs(A_ep + B_it + C);
%         denomOfDist = A.^2 + B.^2;
%         denomOfDist = sqrt(denomOfDist);
%         dist21 = numerOfDist./denomOfDist;
%     end
%     % =================================================================
%     
%     %> use distance to epipolar line to decide inlier/outlier
%     num_tracks = size(covisible_feature_tracks, 1);
%     forward_inlier_ratio = 0;
%     backward_inlier_ratio = 0;
%      
% %     for p = 1:size(dist12,1)
% %         inlier_idx_forward = find(dist12(p,:)<PARAMS.INLIER_REPROJ_ERR_THRESH);
% %         inlier_idx_backward = find(dist21(p,:)<PARAMS.INLIER_REPROJ_ERR_THRESH);
% %         %consistency_inlier_num = size(intersect(inlier_idx_forward, inlier_idx_backward), 2);
% %         %disp(consistency_inlier_num/num_tracks);
% %         %disp(size(inlier_idx_forward, 2)/num_tracks);
% %         %disp(size(inlier_idx_backward, 2)/num_tracks);
% %         
% %         forward_inlier_ratio = forward_inlier_ratio + size(inlier_idx_forward, 2)/num_tracks;
% % %         backward_inlier_ratio = backward_inlier_ratio + size(inlier_idx_backward, 2)/num_tracks;
% %     end
%     
%     inlier_idx_forward = find(dist12(end,:)<PARAMS.INLIER_REPROJ_ERR_THRESH);
%     inlier_idx_backward = find(dist21(end,:)<PARAMS.INLIER_REPROJ_ERR_THRESH);
%     %disp(size(inlier_idx_forward, 2)/size(forward_inlierIndx,2));
%     %disp(size(inlier_idx_backward, 2)/size(backward_inlierIndx,2));
%     
% %     total_inlier_ratio = forward_inlier_ratio + backward_inlier_ratio;
% %     total_inlier_ratio = total_inlier_ratio / (2*(covisible_frames-1));
% %     disp(total_inlier_ratio);
%     
%     if PARAMS.DRAW_EPIPOLAR_LINE
%         img1 = imread(imgFileName(start_fr,1));
%         img2 = imread(imgFileName(start_fr+1,1));
%         
%         %> compute the coefficients of the epipolar line function
%         draw_feature_index = 1;
%         a_img2 = E(1,:,1)*feat1(:, draw_feature_index);
%         b_img2 = E(2,:,1)*feat1(:, draw_feature_index);
%         c_img2 = E(3,:,1)*feat1(:, draw_feature_index);
% 
%         yMin_img2 = -c_img2./b_img2;
%         yMax_img2 = (-c_img2 - a_img2*imgCols) ./ b_img2;
% 
%         figure, img4 = [img1, img2];  
%         colormap('gray');
%         imagesc(img4);
% 
%         hold on;
%         line([imgCols, imgCols+imgCols], ...
%              [round(yMin_img2), round(yMax_img2)], 'Color', 'r');
%         plot(feat1(1,draw_feature_index), feat1(2,draw_feature_index), 'r+');
%         plot(feat2(1,draw_feature_index)+imgCols, feat2(2,draw_feature_index), 'r+');
%         hold off;
%     end


%> draw inlier ratio v.s. track lengths
% if PARAMS.DRAW_INLIER_RATIO_TRACKLENGTH
%     figure;
%     set_tracks = min_covisible_frames:max_covisible_frames;
%     plot(set_tracks, inlier_ratio(:,1), 'bo-');
%     xlabel('track length');
%     ylabel('inlier ratio');
%     legend(featureType);
%     set(gcf,'color','w');
% end

%> draw feature tracks and gamma tracks
if PARAMS.DRAW_TRACKS
    self_drawFeatureTracks(imgFileName(start_fr,1), covisible_frames, covisible_feature_tracks, covisible_gamma_tracks, -1);
end

fclose(printToFile);
fclose(inlier_ratio_printToFile);