#!pip install -r tapnet/requirements_inference.txt

# Commented out IPython magic to ensure Python compatibility.
# @title Download Model {form-width: "25%"}

# %mkdir tapnet/checkpoints
#!wget -P tapnet/checkpoints https://storage.googleapis.com/dm-tapnet/tapir_checkpoint_panning.npy
# %ls tapnet/checkpoints

# @title Imports {form-width: "25%"}

import jax
import jax.numpy as jnp
import haiku as hk
#import mediapy as media
import numpy as np
import tree
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from PIL import Image
import os
from os import listdir
import imageio.v3 as iio
import glob
from tapnet import tapir_model
from tapnet.utils import transforms
from tapnet.utils import viz_utils
from tapnet.utils import model_utils
from scipy.io import savemat
import mediapy as media

# @title Build Model {form-width: "25%"}

def build_model(frames, query_points):
  """Compute point tracks and occlusions given frames and query points."""
  model = tapir_model.TAPIR(bilinear_interp_with_depthwise_conv=False, pyramid_level=0)
  outputs = model(
      video=frames,
      is_training=False,
      query_points=query_points,
      query_chunk_size=64,
  )
  return outputs


"""## Load and Build Model"""
def inference(frames, query_points):
  """Inference on one video.

  Args:
    frames: [num_frames, height, width, 3], [0, 255], np.uint8
    query_points: [num_points, 3], [0, num_frames/height/width], [t, y, x]

  Returns:
    tracks: [num_points, 3], [-1, 1], [t, y, x]
    visibles: [num_points, num_frames], bool
  """
  # Preprocess video to match model inputs format
  frames = model_utils.preprocess_frames(frames)
  num_frames, height, width = frames.shape[0:3]
  query_points = query_points.astype(np.float32)
  frames, query_points = frames[None], query_points[None]  # Add batch dimension

  # Model inference
  rng = jax.random.PRNGKey(42)
  outputs, _ = model_apply(params, state, rng, frames, query_points)
  outputs = tree.map_structure(lambda x: np.array(x[0]), outputs)
  tracks, occlusions, expected_dist = outputs['tracks'], outputs['occlusion'], outputs['expected_dist']

  # Binarize occlusions
  visibles = model_utils.postprocess_occlusions(occlusions, expected_dist)
  return tracks, visibles



def sample_grid_points(frame_idx, height, width, stride=1):
  """Sample grid points with (time height, width) order."""
  points = np.mgrid[stride//2:height:stride, stride//2:width:stride]
  points = points.transpose(1, 2, 0)
  out_height, out_width = points.shape[0:2]
  frame_idx = np.ones((out_height, out_width, 1)) * frame_idx
  points = np.concatenate((frame_idx, points), axis=-1).astype(np.int32)
  points = points.reshape(-1, 3)  # [out_height*out_width, 3]
  return points



#checkpoint_path = '/gpfs/data/bkimia/cchien3/TAPIR/tapnet/checkpoints/tapir_checkpoint_panning.npy'
checkpoint_path = "/users/zqiwu/tapir_code/tapnet/checkpoints/tapir_checkpoint_panning.npy"
ckpt_state = np.load(checkpoint_path, allow_pickle=True).item()
params, state = ckpt_state['params'], ckpt_state['state'] 

model = hk.transform_with_state(build_model)
model_apply = jax.jit(model.apply)

#images_dir_path = "/gpfs/data/bkimia/cchien3/TAPIR/tapnet/examplar_videos/horsejump_imgs/"
images_dir_path = "/users/zqiwu/tapir_code/KITTI_gray/sequences/00/image_1"
num_of_imgs = 25
num_of_channels = 3

resize_height = 512  # @param {type: "integer"}
resize_width = 512  # @param {type: "integer"}
orig_height = 376
orig_width = 1241
stride = 16  # @param {type: "integer"}
query_frame = 0  # @param {type: "integer"}

################# put all images in a matrix ##########################
total_num_of_imgs = len(glob.glob(os.path.join(images_dir_path, '*.png')))
total_orig_frames = np.zeros((total_num_of_imgs, orig_height, orig_width, num_of_channels), dtype=np.uint8)
total_frames = np.zeros((total_num_of_imgs, resize_height, resize_width, num_of_channels), dtype=np.uint8)
total_frames_idx = 0
total_tracks_list = []

for img_path in glob.glob(os.path.join(images_dir_path, '*.png')):
    img = img_arr = iio.imread(img_path)
    img_arr = np.stack((img_arr,)*3, axis=-1)
    height, width = img_arr.shape[0:2]
    # original images
    img_orig = np.array(Image.fromarray(img_arr.astype(np.uint8)))
    total_orig_frames[total_frames_idx] = np.array(img_orig)
    # resized images
    img_ = np.array(Image.fromarray(img_arr.astype(np.uint8)).resize((resize_height, resize_width)))
    total_frames[total_frames_idx] = np.array(img_)
    total_frames_idx += 1

#total_frames = total_frames[:30]
#total_num_of_imgs = 30
############### loop over all covisible frames ##########################
for img_num in range(total_num_of_imgs - 25):
  print('at frame ' + str(img_num))
  frames = np.zeros(shape=(25, resize_height, resize_width, num_of_channels)).astype(np.uint8)
  frames_idx = 0
  for frames_idx in range(25):
      height, width = img_arr.shape[0:2]
      if frames_idx == 0:
            orig_frames = np.zeros(shape=(25, orig_height, orig_width, num_of_channels)).astype(np.uint8)
      orig_frames[frames_idx] = total_orig_frames[frames_idx+img_num]
      frames[frames_idx] = total_frames[frames_idx+img_num]
      frames_idx = frames_idx + 1

  query_points = sample_grid_points(query_frame, resize_height, resize_width, stride)
  batch_size = 64
  tracks = []
  visibles = []
  for i in range(0,query_points.shape[0],batch_size):
    query_points_chunk = query_points[i:i+batch_size]
    num_extra = batch_size - query_points_chunk.shape[0]
    if num_extra > 0:
      query_points_chunk = np.concatenate([query_points_chunk, np.zeros([num_extra, 3])], axis=0)
    tracks2, visibles2 = inference(frames, query_points_chunk)
    if num_extra > 0:
      tracks2 = tracks2[:-num_extra]
      visibles2 = visibles2[:-num_extra]
    tracks.append(tracks2)
    visibles.append(visibles2)
  tracks=jnp.concatenate(tracks, axis=0)
  visibles=jnp.concatenate(visibles, axis=0)

  tracks = transforms.convert_grid_coordinates(tracks, (resize_width, resize_height), (width, height))
  tracks = np.maximum(tracks, 0.0)
  tracks = np.minimum(tracks, [orig_frames.shape[2], orig_frames.shape[1]])

  np.set_printoptions(suppress=True)

  # The inlier point threshold for ransac, specified in normalized coordinates
  # (points are rescaled to the range [0, 1] for optimization).
  ransac_inlier_threshold = 0.07  # @param {type: "number"}
  # What fraction of points need to be inliers for RANSAC to consider a trajectory
  # to be trustworthy for estimating the homography.
  ransac_track_inlier_frac = 0.95  # @param {type: "number"}
  # After initial RANSAC, how many refinement passes to adjust the homographies
  # based on tracks that have been deemed trustworthy.
  num_refinement_passes = 2  # @param {type: "number"}
  # After homographies are estimated, consider points to be outliers if they are
  # further than this threshold.
  foreground_inlier_threshold = 0.07  # @param {type: "number"}
  # After homographies are estimated, consider tracks to be part of the foreground
  # if less than this fraction of its points are inliers.
  foreground_frac = 0.6  # @param {type: "number"}

  occluded = 1.0 - visibles
  homogs, err, canonical = viz_utils.get_homographies_wrt_frame(
      tracks,
      occluded,
      [width, height],
      thresh=ransac_inlier_threshold,
      outlier_point_threshold=ransac_track_inlier_frac,
      num_refinement_passes=num_refinement_passes,
  )

  inliers = (err < np.square(foreground_inlier_threshold)) * visibles
  inlier_ct = np.sum(inliers, axis=-1)
  ratio = inlier_ct / np.maximum(1.0, np.sum(visibles, axis=1))
  is_fg = ratio <= foreground_frac
  tracks = tracks[is_fg]
  total_tracks_list.append(tracks)
  np.set_printoptions(suppress=True, formatter={'float_kind':'{:0.2f}'.format})
  file_name = '/users/zqiwu/tapir_code/feature_points/TAPIR_feature_points_' + str(img_num) + '.mat'
  savemat(file_name, {'matrix_3d': tracks})

  video = viz_utils.plot_tracks_tails(
      orig_frames,
      tracks,#[is_fg],
      occluded[is_fg],
      homogs
  )
  media.show_video(video, fps=24)
