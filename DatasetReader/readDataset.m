
% -- function of reading dataset --
% -- input: dataset name, sequence name --
% -- output: all sequence of T, R, timestamp --
function [all_T, all_R, time_per_fr, imgFileName] = readDataset(datasetName, sequenceName, imgCategory)
    if strcmp(datasetName, 'KITTI')
        sequence = sequenceName;
        % -- image data directories --
        imgFolder = '/home/chchien/datasets/KITTI/sequences-gray/';
        imgDir = strcat(imgFolder, sequence);
        
        % -- read real poses from KITTI ground truths --
        poseFolder = '/home/chchien/BrownU/research/Differential-Visual-Odometry/';
        dataset = 'KITTI-gt/poses/';
        postfix = '.txt';
        
        % -- read R, T of each frame --
        pose_FileName = strcat(poseFolder, dataset, sequence, postfix);
        [all_R, all_T] = read_groundtruths_kitti(pose_FileName);
        
        % -- read time stamp of each pose --
        timeFile = '/times.txt';
        times_FileName = fullfile(imgDir, timeFile);
        timesFileRd = fopen(times_FileName, 'r');
        ldata = textscan(timesFileRd, '%s', 'CollectOutput', true);
        line = string(ldata{1});
        time_per_fr = str2double(line);
        time_per_fr = time_per_fr';
        
        % -- read all image directories as a set of strings (imgFileName) --
        imgFileName = strings(size(all_T,2), 1);
        for i = 0:size(all_T, 2)-1
            imgName = num2str(i,'%06.f.png');
            imgFileName(i+1, 1) = fullfile(imgFolder, sequence, imgCategory, imgName);
        end
        
    elseif strcmp(datasetName, 'EuRoC')
        sequence = sequenceName;
        
        % -- read associated files --
        dataFolder = '/home/chchien/datasets/EuRoC/';
        img_gt_align_files = 'associate_img_gt.txt';
        align_full_dir = fullfile(dataFolder, sequence, img_gt_align_files);

        % -- extract timestamps, body frame ground truths, and image file names 
        gt_img = fopen(align_full_dir, 'r');
        ldata = textscan(gt_img, '%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%s', 'CollectOutput', true );
        time_gt = ldata{1,1};
        img_filename = ldata{1,2};

        % -- retrieve time --
        sample_time = (time_gt(2,1) - time_gt(1,1))*1e-9;   % -- seconds --
        time_per_fr = zeros(1, size(time_gt,1)); 
        for i = 1:size(time_gt,1)
            time_per_fr(1,i) = (i-1)*sample_time;
        end
        numOfPoses = size(time_gt, 1);

        % -- transformation wrt body frame --
        sensor_filename = 'cam0/sensor.yaml';
        sensor_dir = strcat(dataFolder, sequence, sensor_filename);
        sensorContent = ReadYaml(sensor_dir);
        Tr_arr = cell2mat(sensorContent.T_BS.data);
        Tr_body2cam = reshape(Tr_arr, 4, 4)';

        % -- retrieve R and T of the body frame --
        T_body = zeros(3, numOfPoses);
        R_body = zeros(3, 3, numOfPoses);
        Tr_body = zeros(4, 4, numOfPoses);
        Tr_cam_bw = zeros(4, 4, numOfPoses);
        Tr_cam = zeros(4, 4, numOfPoses);
        Tr_cam_cw = zeros(4, 4, numOfPoses);
        dcm = zeros(3, 3, numOfPoses);
        for p = 1:numOfPoses
            % -- retrieve R and T from ground truths ( body frame in world
            % coordinate ) --
            T_body(:, p) = time_gt(p, 2:4);
            quat = time_gt(p, 5:8);
            rotm = quat2rotm(quat);
            R_body(:,:,p) = rotm;
            Tr_body(1:3, 1:3, p) = R_body(:,:,p);
            Tr_body(1:3, 4, p) = T_body(:,p);
            Tr_body(4, 4, p) = 1;

            % -- move the body frame to the origin --        
            Tr_cam_bw(1:3,1:3,p) = Tr_body(1:3,1:3,1)' * Tr_body(1:3,1:3,p);
            Tr_cam_bw(1:3,4,p) = Tr_body(1:3,1:3,1)' * (Tr_body(1:3,4,p) - Tr_body(1:3,4,1));
            Tr_cam_bw(4,4,p) = 1;

            % -- transfer body frame to left camera frame --
            Tr_cam(1:3,4,p) = Tr_cam_bw(1:3,4,p) + Tr_body2cam(1:3,4);
            Tr_cam(1:3,1:3,p) = Tr_body2cam(1:3,1:3) * Tr_cam_bw(1:3,1:3,p);
            Tr_cam(4,4,p) = 1;

            % -- move the left camera frame to the origin --        
            Tr_cam_cw(1:3,1:3,p) = Tr_cam(1:3,1:3,1)' * Tr_cam(1:3,1:3,p);
            Tr_cam_cw(1:3,4,p) = Tr_cam(1:3,1:3,1)' * (Tr_cam(1:3,4,p) - Tr_cam(1:3,4,1));
            Tr_cam_cw(4,4,p) = 1;
        end

        % -- store R, T from transformation matrix --
        all_T(:,:) = Tr_cam_cw(1:3,4,:);
        all_R = Tr_cam_cw(1:3,1:3,:);

        % -- TODO: read all images and return their directories --
        imgFileName = strings(numOfPoses, 1);
        for i = 1:size(all_T, 2)
            imgName = string(img_filename{i});
            imgFileName(i+1, 1) = fullfile(dataFolder, sequence, 'cam0/data/', imgName);
        end
        
    elseif strcmp(datasetName, 'BPOD')
        sequence = sequenceName;
        % -- image data directories --
        imgFolder = '/home/chchien/datasets/BPOD/bpod_pedestrian_dataset/gt_415/';
        imgDir = strcat(imgFolder, sequence);
        
        % -- read real poses from KITTI ground truths --
        poseFolder = '/home/chchien/datasets/BPOD/bpod_pedestrian_dataset/';
        dataset = 'gt_415/';
        postfix = '.txt';
        
        % -- read R, T of each frame --
        pose_FileName = strcat(poseFolder, dataset, sequence, postfix); 
        gt_bpod = fopen(pose_FileName, 'r');
        ldata = textscan(gt_bpod, '%f\t%f\t%f', 'CollectOutput', true );
        time_pose_gt = ldata{1,1};
        
        time_per_fr = time_pose_gt(:,1);
        time_per_fr = time_per_fr.*0.000001;
        all_T = time_pose_gt(:,2:3);
        
        % -- TODO: requires BPOD images and rotation data --
        all_R = zeros(size(all_T));
        imgFileName = strings(size(time_per_fr));
        
%         img_filename = ldata{1,2};
%         
%         % -- read time stamp of each pose --
%         timeFile = '/times.txt';
%         times_FileName = fullfile(imgDir, timeFile);
%         timesFileRd = fopen(times_FileName, 'r');
%         ldata = textscan(timesFileRd, '%s', 'CollectOutput', true);
%         line = string(ldata{1});
%         time_per_fr = str2double(line);
%         time_per_fr = time_per_fr';
%         
%         % -- read all image directories as a set of strings (imgFileName) --
%         imgFileName = strings(size(all_T,2), 1);
%         for i = 0:size(all_T, 2)-1
%             imgName = num2str(i,'%06.f.png');
%             imgFileName(i+1, 1) = fullfile(imgFolder, sequence, imgCategory, imgName);
%         end
    end
end

