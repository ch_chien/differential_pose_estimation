function [featureDescriptions, featurePoints, rows, cols] = self_feature_extractor(imgName, featureType)

    % -- load image and convert it to gray scale --
    img_load = imread(imgName);
    [rows, cols, channels] = size(img_load);
    if channels > 1
        grayScaleImg = im2double(rgb2gray(img_load));
    else
        grayScaleImg = im2double(img_load);
    end
    
    % -- extract Features --
    if strcmp(featureType, 'ORB')
        keyPointsObj = detectORBFeatures(grayScaleImg);
        [binaryDescriptions, featurePoints] = extractFeatures(grayScaleImg, keyPointsObj);
        featureDescriptions = binaryDescriptions.Features;
    elseif strcmp(featureType, 'SURF')
        keyPointsObj_prev = detectSURFFeatures(grayScaleImg);
        [featureDescriptions, featurePoints] = extractFeatures(grayScaleImg, keyPointsObj_prev);
    elseif strcmp(featureType, 'vlfeat-SIFT')
        grayScaleImg_single = single(grayScaleImg);
        %[featurePoints, featureDescriptions] = vl_sift(grayScaleImg_single, 'PeakThresh', vlfeat_params.SiftThreshPeak, 'edgethresh', vlfeat_params.SiftThreshEdge);
        [fpts, fdes] = vl_sift(grayScaleImg_single);
        featureDescriptions = fdes';
        featurePoints = fpts';
    end

end